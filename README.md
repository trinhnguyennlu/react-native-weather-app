# React Native Weather App

React native using Open Weather API - http://openweathermap.org/api

A Weather app build using:

- React Native
- Vector Icons
- Navigation
- Geolocation
- Moment
- Picker
- Redux
- React-redux
- React-thunk

## Preview

![Preview App](https://gitlab.com/trinhnguyennlu/react-native-weather-app/raw/master/src/assets/images/preview.gif "Preview")

## Installation

1. Run npm install
2. Run react-native start
3. Run react-native run-android or react-native-ios
4. Make sure android sdk is configure properly if you are running this application on AVD (android virtual devices.) OR Point a emulator or phone to the address to run.

## Data

- Weather data provided by Open Weather API - http://openweathermap.org/api
- Background images using Svg library
- Icons from Vector Ions - https://oblador.github.io/react-native-vector-icons
