import React from 'react';
import { Text, View, Animated, StyleSheet, Dimensions, SafeAreaView } from 'react-native';

import styles from './Page5.style';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import { Global, Colors } from '../../styles/index.style';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Svg, Path } from 'react-native-svg';

interface Props {
  navigation: NavigationScreenProp<NavigationState>;
}

const WIDTH = Dimensions.get('screen').width;

const Page5 = ({ navigation }: any) => {
  return (
    <SafeAreaView style={styles.container}>
      <Svg style={{ flex: 1 }} height={550} width={WIDTH} viewBox="0 0 375 600" preserveAspectRatio='none'>
        <Path
          d="M-17.5 520.5C31.5 350.5 220.5 290 375 89C447.5 -285 375 644 375 644H0C0 644 -66.5 724.5 -17.5 378.5Z"
          fill={Colors.easternBlue}
          stroke={Colors.easternBlue}
        />
      </Svg>
      <View style={{ flex: 1, backgroundColor: Colors.easternBlue }}></View>
      <View style={{ position: 'absolute', width: '100%' }}>
        <View style={styles.wrapper}>
          <Text style={[Global.textColor, Global.fontSizeMedium]}>Tuesday</Text>
          <Text style={Global.textColor}>May 20 2020</Text>
          <View style={styles.hairline} />
          <Text style={[Global.textColor, styles.time]}>8:43</Text>
          <View style={styles.wrapperContent}>
            <Icon name="snowflake-o" style={styles.icon}></Icon>
            <Text style={[Global.textColor, styles.temperature]}>11&#8451;</Text>
            <Text style={[Global.textColor, Global.fontSizeMedium]}>Snow</Text>
          </View>
        </View>
        <View style={styles.progressBar}>
          <Animated.View style={[StyleSheet.absoluteFill, styles.pecents]} />
        </View>
        <View style={styles.wrapper}>
          <Text style={[Global.textColor, { textAlign: 'center' }]}>-1&#8451;/+1&#8451;</Text>
          <Text style={[Global.textColor, styles.city, Global.fontSizeMedium]}>City</Text>
          <Text style={[Global.textColor, styles.country, Global.fontSizeMedium]}>Country</Text>
        </View>
      </View>
    </SafeAreaView>
  )
};

export default Page5;
