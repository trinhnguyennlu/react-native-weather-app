import { StyleSheet } from 'react-native';
import { Colors } from '../../styles/index.style';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapperIcon: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
    paddingTop: 20,
    paddingRight: 20,
  },
  iconSetting: {
    fontSize: 20,
    color: Colors.white,
    paddingLeft: 5
  },
  hairline: {
    backgroundColor: Colors.white,
    height: 2,
    width: 150,
    marginTop: 5,
    marginBottom: 5
  },
  flexContainer: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center'
  },
  flexItem: {
    textAlign: 'center',
    fontSize: 20
  },
  icon: {
    fontSize: 150,
    color: Colors.white
  },
  wrapper: {
    width: '100%'
  },
  wrapperContent: {
    padding: 20
  },
  mainContent: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 20
  },
  time: {
    fontSize: 30
  },
  temperature: {
    fontSize: 40,
    marginTop: 30,
    marginBottom: 10
  },
  progressBar: {
    height: 10,
    width: '100%',
    borderColor: Colors.white,
    borderWidth: 1,
  },
  pecents: {
    backgroundColor: Colors.white,
    width: '50%'
  },
  city: {
    textAlign: 'right',
    marginTop: 20
  },
  country: {
    textAlign: 'right',
    marginBottom: 10
  },
  picker: {
    color: Colors.white,
    width: '49%',
    flex: 1,
    alignSelf: 'flex-end',
  },
});
