import React, { Component, useState } from 'react';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import { View, Text, Animated, StyleSheet, SafeAreaView, Dimensions, Alert, ScrollView } from 'react-native';
import styles from './Page1.style';
import { Global } from '../../styles/index.style';
import Icon from 'react-native-vector-icons/Fontisto';
import { Svg, Path } from 'react-native-svg';
import Geolocation from '@react-native-community/geolocation';
import {connect} from 'react-redux';
import {getPageData} from '../../actions/pageActions';
import moment from 'moment';
import Geocoder from 'react-native-geocoder';
import { weatherConditions } from '../../utils/WeatherCondition';
import {Picker} from '@react-native-community/picker';

interface Props {
  navigation: NavigationScreenProp<NavigationState>;
  getPageData: any,
}

const WIDTH = Dimensions.get('screen').width;

class Page1 extends Component<Props> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: props.data.pageList,
      loading: props.data.loading,
      pickerCityValue: 'default',
      pickerCountryValue: 'default'
    }
  }

  componentDidMount() {
    Geolocation.getCurrentPosition(
      (position) => {
        Geocoder.geocodePosition({lat: position.coords.latitude,lng: position.coords.longitude})
        .then(res => {
          this.props.getPageData(res[0].locality);
        })
        .catch(err =>  Alert.alert(err.message));
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    )
  }

  static getDerivedStateFromProps(nextProps: any, prevState: any) {
    if(nextProps.data !== prevState.data) {
      return {
        data: nextProps.data.pageList,
        loading: nextProps.data.loading
      };
    }
    return null;
  }

  onChangeCityHandler(value: any): void {
    this.setState({pickerCityValue: value});
    if (value != 'default') {
      this.props.getPageData(value);
    }
  }

  onChangeCountryHandler(value: any): void {
    this.setState({pickerCountryValue: value});
  }

  render (){
    const { loading, data, pickerCityValue, pickerCountryValue } = this.state;
    let city = [
      { label: 'Ho Chi Minh', value: 'Ho Chi Minh' },
      { label: 'Ha Noi', value: 'Hanoi' },
      { label: 'Hai Phong', value: 'Haiphong' },
      { label: 'Da Nang', value: 'Da Nang' },
      { label: 'Can Tho', value: 'Can Tho' },
    ];
    let country = [
      { label: 'Viet Nam', value: 'VN' }
    ];

    return (
      <SafeAreaView style={styles.container}>
        { loading ? (
          <View style={styles.flexContainer}>
            <Text style={styles.flexItem}>Fetching The Weather</Text>
          </View>
        ) : (
          <View style={[{height: '100%'}, {backgroundColor: weatherConditions[data.weather[0].main].brightColor}]}>
            <Svg style={{ flex: 1 }} height={550} width={WIDTH} viewBox="0 0 345 600" preserveAspectRatio='none'>
              <Path
                d="M-17.5 550.5C31.5 350.5 250.5 290 375 89C447.5 -285 375 644 375 644H0C0 644 -66.5 724.5 -17.5 378.5Z"
                fill={weatherConditions[data.weather[0].main].darkColor}
                stroke={weatherConditions[data.weather[0].main].darkColor}
              />
            </Svg>
            <View style={{ flex: 1, backgroundColor: weatherConditions[data.weather[0].main].darkColor }}></View>
            <ScrollView style={[styles.wrapper, StyleSheet.absoluteFill]}>
              <View style={styles.wrapperIcon}>
                <Icon name="player-settings" style={styles.iconSetting}></Icon>
                <Icon name="clock" style={styles.iconSetting}></Icon>
                <Icon name="more-v-a" style={styles.iconSetting}></Icon>
              </View>
              <View style={styles.wrapperContent}>
                <Text style={[Global.textColor, Global.fontSizeMedium]}>{moment().format('dddd')}</Text>
                <Text style={Global.textColor}>{moment().format('LL')}</Text>
                <View style={styles.hairline} />
                <Text style={[Global.textColor, styles.time]}>{moment().format('LT')}</Text>
                <View style={styles.mainContent}>
                  <Icon name={weatherConditions[data.weather[0].main].icon} style={styles.icon}></Icon>
                  <Text style={[Global.textColor, styles.temperature]}>{data.main.temp}&#8451;</Text>
                  <Text style={[Global.textColor, Global.fontSizeMedium]}>{weatherConditions[data.weather[0].main].title}</Text>
                </View>
              </View>
              <View style={styles.progressBar}>
                <Animated.View style={[StyleSheet.absoluteFill, styles.pecents]} />
              </View>
              <View style={styles.wrapperContent}>
                <Text style={[Global.textColor, { textAlign: 'center' }]}>-1&#8451;/+1&#8451;</Text>
                <Picker
                  selectedValue={pickerCityValue}
                  mode="dropdown"
                  style={styles.picker}
                  onValueChange={(itemValue, itemIndex) => this.onChangeCityHandler(itemValue)}>
                  <Picker.Item label="Select a City" value="default" />
                  {city.map ((item)=><Picker.Item label={item.label} value={item.value} key={item.value}/>)}
                </Picker>
                <Picker
                  selectedValue={pickerCountryValue}
                  mode="dropdown"
                  style={styles.picker}
                  onValueChange={(itemValue, itemIndex) => this.onChangeCountryHandler(itemValue)}>
                  <Picker.Item label="Select a Country" value="default" />
                  {country.map ((item)=><Picker.Item label={item.label} value={item.value} key={item.value}/>)}
                </Picker>
              </View>
            </ScrollView>
          </View>
        )}
      </SafeAreaView>
    )
  }
};

const mapStateToProps = (state: any) => {
  return {
    data: state.pageData
  }
}

export default connect(mapStateToProps, {getPageData})(Page1);