import { StyleSheet } from 'react-native';

import { Colors } from '../../styles/index.style';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.jungleGreen,
    flex: 1,
  },
  hairline: {
    backgroundColor: Colors.white,
    height: 2,
    width: 150,
    marginTop: 5,
    marginBottom: 5
  },
  icon: {
    fontSize: 150,
    color: Colors.white
  },
  wrapper: {
    padding: 20
  },
  wrapperContent: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 20
  },
  time: {
    fontSize: 30
  },
  temperature: {
    fontSize: 40,
    marginTop: 30,
    marginBottom: 10
  },
  progressBar: {
    height: 10,
    width: '100%',
    borderColor: Colors.white,
    borderWidth: 1,
  },
  pecents: {
    backgroundColor: Colors.white,
    width: '50%'
  },
  city: {
    textAlign: 'right',
    marginTop: 20
  },
  country: {
    textAlign: 'right',
    marginBottom: 10
  }
});
