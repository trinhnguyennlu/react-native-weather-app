import React from 'react';
import {
  createBottomTabNavigator,
  createAppContainer
} from 'react-navigation';

import BottomTab from './components/BottomTab';
import Page1 from './screens/Page1';
import Page2 from './screens/Page2';

const AppNavigator = createBottomTabNavigator(
  {
    Page1: Page1,
    Page2: Page2
  },
  {
    tabBarComponent: ( props: any ) => <BottomTab { ...props } />,
  }
)

export default createAppContainer(AppNavigator);
