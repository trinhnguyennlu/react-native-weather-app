import { Colors } from '../styles/index.style';

export const textColor = {
  color: Colors.white,
}
export const fontSizeMedium = {
  fontSize: 20
}

export const textAlignCenter = {
  textAlign: 'center'
}