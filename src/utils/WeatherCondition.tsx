import { Colors } from "../styles/index.style";

export const weatherConditions = {
	Rain: {
		darkColor: Colors.gossamer,
		brightColor: Colors.persianGreen,
		title: 'Rainy',
		icon: 'rain'
  },
  Clear: {
		darkColor: Colors.cinnabar,
		brightColor: Colors.flamingo,
		title: 'Sunny',
		icon: 'day-sunny'
	},
	Thunderstorm: {
		darkColor: Colors.atoll,
		brightColor: Colors.denim,
		title: 'Storm',
		icon: 'lightning'
	},
	Clouds: {
		darkColor: Colors.sushi,
		brightColor: Colors.pear,
		title: 'Cloudy',
		icon: 'day-cloudy'
	},

	Snow: {
		darkColor: Colors.easternBlue,
		brightColor: Colors.curiousBlue,
		title: 'Snow',
		icon: 'snowflake-2'
	},
}