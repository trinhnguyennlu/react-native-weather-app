import { combineReducers } from "redux";
import pageDataReducer from "./pageReducer";

const rootReducer = combineReducers({
  pageData: pageDataReducer,
});

export default rootReducer;