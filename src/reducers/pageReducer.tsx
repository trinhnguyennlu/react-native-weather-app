import { GET_DATA_START, GET_DATA_FINISHED, GET_DATA_FAILURE } from "../constants";

const initialState = {
  loading: false,
  pageList: [],
};
const pageDataReducer = (state = initialState, action: { type: any; payload: any; }) => {
  switch (action.type) {
    case GET_DATA_START:
      return {
        ...state,
        loading: true
      };
    case GET_DATA_FINISHED:
      return {
        ...state,
        loading: false,
        pageList: action.payload
      };
    case GET_DATA_FAILURE:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
};

export default pageDataReducer;