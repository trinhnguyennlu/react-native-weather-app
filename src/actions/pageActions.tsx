import { GET_DATA_START, GET_DATA_FAILURE, GET_DATA_FINISHED, API_KEY } from "../constants";
import axios from 'axios';

export const getPageData = (cityName: string) => {
  return async (dispatch: (arg0: { type: string; }) => void) => {
    try {
      dispatch(getPageRequest());
      const starWarsPromise = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${API_KEY}&units=metric`);
      dispatch(setPageData(starWarsPromise.data));
    } catch (err) {
      dispatch(getDataFailure(err));
    }
  };
};

export const getPageRequest = () => {
  return {
    type: GET_DATA_START,
  };
};

export const setPageData = (data: object) => {
  return {
    type: GET_DATA_FINISHED,
    payload: data
  };
};

export const getDataFailure = (err: object) => {
  return {
    type: GET_DATA_FAILURE,
    payload: err
  };
};