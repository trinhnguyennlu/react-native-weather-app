import React from 'react';
import { BottomTabBar } from 'react-navigation-tabs';
import { Colors } from '../../styles/index.style';
import styles from './BottomTab.style';

const BottomTab = (props: any) => {
  return (
    <BottomTabBar
      {...props}
      activeTintColor={Colors.amaranth}
      labelStyle={{fontSize: 16}}
      style={styles.container}
    />
  );
};

export default BottomTab;