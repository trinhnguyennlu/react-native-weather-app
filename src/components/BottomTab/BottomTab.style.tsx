import {StyleSheet, ViewStyle, TextStyle} from 'react-native';
import { Colors } from '../../styles/index.style';

interface Styles {
  container: ViewStyle;
  title: TextStyle;
}

export default StyleSheet.create<Styles>({
  container: {
    paddingBottom: 12,
    backgroundColor: Colors.capeHoney,
    borderTopColor: Colors.scorpion,
  },
  title: {
    color: Colors.black,
  },
});
